﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MotoRally
{
    class Player
    {
        // Закрытые
        private Texture2D Texture;
        private Rectangle Rectangle;
        private Vector2 Original_position, Velosity;
        private KeyboardState KS;
        private Color Colour;
        private int FrameHeight, FrameWidth, FrameCurrent;
        private float TimerFrame, TimerImmunity, TimerJump, TimerAnimaion;
        // Открытые
        public Vector2 Position;
        public Rectangle Rectangle_Interaction;
        public string Name = "Игрок", PlayerRoute, Status;
        public int HP = 5, XP = 0;
        public int SpeedPrimary = 4, SpeedUp;
        public bool FlagImmunity, FlagJumpUp, FlagJumpDown;

        public Player(Texture2D newTexture, Vector2 newPosition)
        {
            Texture = newTexture;
            Position = newPosition;
            FrameHeight = 95;
            FrameWidth = 89;
            PlayerRoute = "C1";
            Status = "Live";
            FlagImmunity = FlagJumpUp = FlagJumpDown = false;
            TimerFrame = TimerImmunity = TimerJump = 0;
            TimerAnimaion = 100;
            Colour = new Color(255, 255, 255, 255);
            SpeedUp = SpeedPrimary;
        }
        public void Update(GameTime _GameTime)
        {
            Rectangle = new Rectangle(FrameCurrent * FrameWidth, 0, FrameWidth, FrameHeight);
            Original_position = new Vector2(Rectangle.Width / 2, Rectangle.Height / 2);
            Rectangle_Interaction = new Rectangle((int)Position.X - FrameWidth / 2, (int)Position.Y - FrameHeight / 2, FrameWidth, FrameHeight);
            TimerFrame += (float)_GameTime.ElapsedGameTime.TotalMilliseconds;
            TimerJump += (float)_GameTime.ElapsedGameTime.TotalMilliseconds;
            if (HP <= 0) Status = "Dead";
            Velosity.X = SpeedUp;
            Position.X = Position.X + Velosity.X;
            /* Изменение скорости анимация взависимости от скорости движения */
            if (SpeedUp <= 0) TimerAnimaion = 1000;
            if (SpeedUp == 1) TimerAnimaion = 130;
            if (SpeedUp == 4) TimerAnimaion = 100;
            if (SpeedUp == 7) TimerAnimaion = 70;
            if (SpeedUp == 5) TimerAnimaion = 80;
            if (SpeedUp == 8) TimerAnimaion = 50;
            if (SpeedUp == 11) TimerAnimaion = 20;
            /* Анимация движения */
            switch (PlayerRoute)
            {
                case "L":
                    if (TimerFrame >= TimerAnimaion)
                    {
                        if (FrameCurrent < 4) FrameCurrent++;
                        else FrameCurrent = 1;
                        TimerFrame = 0;
                    }
                    break;
                case "R":
                    if (TimerFrame >= TimerAnimaion)
                    {
                        if (FrameCurrent < 4) FrameCurrent++;
                        else FrameCurrent = 1;
                        TimerFrame = 0;
                    }
                    break;
                default:
                    break;
            }
            /* Анимация прыжка */
            if (FlagJumpUp)
            {
                FrameCurrent = 6;
                if (TimerJump > 1)
                {
                    if (Position.Y > 150)
                    {
                        Position.Y = Position.Y - 14;
                        TimerJump = 0;
                    }
                    else
                    {
                        FlagJumpUp = false;
                        TimerJump = 0;
                    }
                }
            }
            /* Управление */
            Control(_GameTime);
            /* Имуннетет от урона */
            if (FlagImmunity)
            {
                TimerImmunity += (float)_GameTime.ElapsedGameTime.TotalSeconds;
                Colour = Color.IndianRed;
                if (TimerImmunity >= 3)
                {
                    TimerImmunity = 0;
                    FlagImmunity = false;
                    Colour = Color.White;
                }
            }
        }
        public void Draw(SpriteBatch _SpriteBatch, SpriteFont _SpriteFont)
        {
            _SpriteBatch.Draw(Texture, Position, Rectangle, Colour, 0f, Original_position, 1.0f, SpriteEffects.None, 0.3f);
        }
        private void Control(GameTime _GameTime)
        {
            KS = Keyboard.GetState();
            if (KS.IsKeyDown(Keys.Left))
            {
                PlayerRoute = "L";
                SpeedUp = SpeedPrimary - 3;
            }
            else
            {
                if (KS.IsKeyDown(Keys.Right))
                {
                    PlayerRoute = "R";
                    SpeedUp = SpeedPrimary + 3;
                }
                else
                {
                    SpeedUp = SpeedPrimary;
                    PlayerRoute = "L";
                }
            }
            if ((KS.IsKeyDown(Keys.Space)) && (FlagJumpUp == false) && (FlagJumpDown == true))
            {
                FlagJumpUp = true;
                FlagJumpDown = false;
            }
        }
    }
}
