﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MotoRally
{
    class UI
    {
        private Texture2D TextureBackground;
        private Rectangle RectangleBackground, RectangleCountHP;
        private Vector2 Position;
        private SpriteFont Text_UI;
        private string Name = "", XP = "";
        private int HP, Indent, SecTime;

        public UI(Texture2D newTextureBackground, Vector2 newPosition, int newFrameHeight, int newFrameWidth, SpriteFont newText_UI)
        {
            TextureBackground = newTextureBackground;
            RectangleBackground = new Rectangle(0, 0, TextureBackground.Width, TextureBackground.Height);
            Position = newPosition;
            Text_UI = newText_UI;
            HP = 0;
            Indent = 0;
        }

        public void Update(GameTime gametime, string newName, int newHP, int newXP, int newSecTime)
        {
            Name = newName;
            HP = newHP;
            XP = newXP.ToString();
            SecTime = newSecTime;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(TextureBackground, RectangleBackground, null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
            spriteBatch.DrawString(Text_UI, Name, new Vector2(10, 0), Color.Black);
            spriteBatch.DrawString(Text_UI, HP.ToString(), new Vector2(370, 0), Color.Black);
            spriteBatch.DrawString(Text_UI, XP, new Vector2(800, 0), Color.Black);
            spriteBatch.DrawString(Text_UI, SecTime.ToString(), new Vector2(920, 0), Color.Black);
        }
    }
}
