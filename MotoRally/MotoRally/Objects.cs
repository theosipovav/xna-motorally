﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MotoRally
{
    class Objects
    {
        protected Texture2D Texture;
        protected Rectangle Rectangle;
        protected Vector2 Original_position;
        protected float Timer, TimerFrame;
        public int FrameHeight, FrameWidth, FrameCurrent;
        public Rectangle Rectangle_Interaction;
        public Vector2 Position;
        public Color Colour;
        public Objects(Texture2D newTexture, Vector2 newPosition)
        {
            Texture = newTexture;
            Position = newPosition;
            Colour = new Color(255, 255, 255, 255);
            FrameCurrent = 0;
            Timer = 0;
        }
        public virtual void Draw(SpriteBatch _SpriteBatch)
        {
            _SpriteBatch.Draw(Texture, Position, Rectangle, Colour, 0f, Original_position, 1.0f, SpriteEffects.None, 0.7f);
        }
    }
    class Platform : Objects
    {
        public Platform(Texture2D newTexture, Vector2 newPosition) : base(newTexture, newPosition)
        {
            FrameHeight = newTexture.Height;
            FrameWidth = newTexture.Width;
        }
        public void Update(GameTime _GameTime)
        {
            Rectangle = new Rectangle(FrameCurrent * FrameWidth, 0, FrameWidth, FrameHeight);
            Original_position = new Vector2();
            Rectangle_Interaction = new Rectangle((int)Position.X, (int)Position.Y, FrameWidth, 20);
        }
    }
    class Other : Objects
    {
        public bool flagUse;
        public Other(Texture2D newTexture, Vector2 newPosition) : base(newTexture, newPosition)
        {
            FrameHeight = newTexture.Height;
            FrameWidth = newTexture.Width;
            flagUse = false;
        }
        public void Update(GameTime _GameTime)
        {
            Rectangle = new Rectangle(FrameCurrent * FrameWidth, 0, FrameWidth, FrameHeight);
            Original_position = new Vector2();
        }

        public virtual void DrawNew(SpriteBatch _SpriteBatch)
        {
            _SpriteBatch.Draw(Texture, Position, Rectangle, Colour, 0f, Original_position, 1.0f, SpriteEffects.None, 0.69f);
        }
    }
    class Points : Objects
    {
        public bool FlagUse;
        public Points(Texture2D newTexture, Vector2 newPosition, float newLayerDepth = 0.3f, int newModWidth = 0, int newModHeight = 0) : base(newTexture, newPosition)
        {
            FrameHeight = FrameWidth = 33;
        }
        public void Update(GameTime _GameTime)
        {
            if (!FlagUse)
            {
                Rectangle = new Rectangle(FrameCurrent * FrameWidth, 0, FrameWidth, FrameHeight);
                Original_position = new Vector2(Rectangle.Width / 2, Rectangle.Height / 2);
                Rectangle_Interaction = new Rectangle((int)Position.X - FrameWidth / 2, (int)Position.Y - FrameHeight / 2, FrameWidth, FrameHeight);
                TimerFrame += (float)_GameTime.ElapsedGameTime.TotalMilliseconds;
                if (TimerFrame >= 50)
                {
                    if (FrameCurrent == 0) FrameCurrent = 1;
                    else FrameCurrent = 0;
                    TimerFrame = 0;
                }
            }
        }
        public new void Draw(SpriteBatch _SpriteBatch, float newDepth = 0.51f)
        {
            if (!FlagUse) _SpriteBatch.Draw(Texture, Position, Rectangle, Colour, 0f, Original_position, 1.0f, SpriteEffects.None, 0.51f);
        }
        public void Delete()
        {
            Position = new Vector2(-1000, -1000);
            Rectangle = Rectangle_Interaction = new Rectangle((int)Position.X, (int)Position.Y, 0, 0);
            FlagUse = true;
        }
        public void NewPositionRND(Random newRND, int A = 700, int B = 12000)
        {
            Position = new Vector2(newRND.Next(A, B), newRND.Next(150, 200));
        }
    }


    class Traps : Objects
    {
        public Traps(Texture2D newTexture, Vector2 newPosition) : base(newTexture, newPosition)
        {
            FrameHeight = newTexture.Height;
            FrameWidth = newTexture.Width;

        }
        public void Update(GameTime _GameTime)
        {
            Rectangle = new Rectangle(FrameCurrent * FrameWidth, 0, FrameWidth, FrameHeight);
            Original_position = new Vector2();
            Rectangle_Interaction = new Rectangle((int)Position.X, (int)Position.Y, FrameWidth, FrameHeight - 70);
        }

    }
}
