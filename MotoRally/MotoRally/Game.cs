using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Xml;
using System.IO;

namespace MotoRally
{
    public class Game : Microsoft.Xna.Framework.Game
    {
        /* ���������� ���������� ������� */
        GraphicsDeviceManager _GraphicsDeviceManager;
        SpriteBatch _SpriteBatch;
        KeyboardState _KeyUP, _KeyDown;
        Viewport _ViewportUI, _ViewportGame, _ViewportMenu;
        Random RND;
        /* ���������� ������� ��� ���� */
        Buttons[] _Buttons = new Buttons[4];
        InputText _InputText;
        /* ���������� ������� ��� ���� */
        Camera _Camera;
        Player _Player;
        UI _UI;
        Song[] Sound = new Song[2];
        bool[] FlagStart = new bool[2];
        List<Platform> _Platforms = new List<Platform>();
        List<Points> _Points = new List<Points>();
        List<Traps> _Traps = new List<Traps>();
        int _SecTime;
        float _FallingTime, _Time;
        bool _FlagFalling, _FlagFallingP;
        /* ���������� �������� ���� */
        enum Structure_Game
        {
            Menu,
            Play,
            Final,
        }
        Structure_Game CurrentGame = Structure_Game.Menu;
        enum Structure_Menu
        {
            Global,
            Rating,
        }
        Structure_Menu CurrentMenu = Structure_Menu.Global;
        public Game()
        {
            _GraphicsDeviceManager = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        protected override void Initialize()
        {
            _ViewportMenu = new Viewport();
            _ViewportMenu.X = 0;
            _ViewportMenu.Y = 0;
            _ViewportMenu.Width = 1024;
            _ViewportMenu.Height = 600;
            _ViewportMenu.MinDepth = 0;
            _ViewportMenu.MaxDepth = 1;
            _ViewportUI = new Viewport();
            _ViewportUI.X = 0;
            _ViewportUI.Y = 0;
            _ViewportUI.Width = 1024;
            _ViewportUI.Height = 50;
            _ViewportUI.MinDepth = 0;
            _ViewportUI.MaxDepth = 1;
            _ViewportGame = new Viewport();
            _ViewportGame.X = 0;
            _ViewportGame.Y = 50;
            _ViewportGame.Width = 1024;
            _ViewportGame.Height = 550;
            _ViewportGame.MinDepth = 0;
            _ViewportGame.MaxDepth = 1;
            _Camera = new Camera();
            base.Initialize();
        }
        protected override void LoadContent()
        {
            /* ������������� ���������� ������� */
            _SpriteBatch = new SpriteBatch(GraphicsDevice);
            IsMouseVisible = true;
            _GraphicsDeviceManager.PreferredBackBufferWidth = 1024;
            _GraphicsDeviceManager.PreferredBackBufferHeight = 600;
            _GraphicsDeviceManager.ApplyChanges();
            /* ������������� ������� ��� ���� (������� ���� / �������) */
            _Buttons[0] = new Buttons(Content.Load<Texture2D>("Button_1"), new Vector2(800, 150));
            _Buttons[1] = new Buttons(Content.Load<Texture2D>("Button_2"), new Vector2(800, 250));
            _Buttons[2] = new Buttons(Content.Load<Texture2D>("Button_3"), new Vector2(800, 350));
            _Buttons[3] = new Buttons(Content.Load<Texture2D>("Button_4"), new Vector2(800, 550));
            _InputText = new InputText(new Vector2(130, 50), Content.Load<SpriteFont>("InputText"));
            /* ������������� ������� ��� ���� ������� */
            _Player = new Player(Content.Load<Texture2D>("Player"), new Vector2(50, 0));
            _UI = new UI(Content.Load<Texture2D>("UI"), new Vector2(0, 0), 0, 0, Content.Load<SpriteFont>("TextUI"));
            RND = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
            Sound[0] = Content.Load<Song>("SoundMenu");
            Sound[1] = Content.Load<Song>("SoundPlay");
            FlagStart[0] = false;
            FlagStart[1] = false;
            /* ������������� ������� ��� ������ */
            if (_Platforms.Count > 0) _Platforms.Clear();
            float _NewX = 0;
            /* ������ 1 ����� */
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_1"), new Vector2(0, 100)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_1"), new Vector2(_NewX + 50, 230)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_2"), new Vector2(_NewX + 50, 330)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_4"), new Vector2(_NewX + 200, 330)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_4"), new Vector2(_NewX + 100, 370)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_3"), new Vector2(_NewX + 100, 230)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_2"), new Vector2(_NewX, 230)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_3"), new Vector2(_NewX + 150, 330)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_2"), new Vector2(_NewX, 330)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_3"), new Vector2(_NewX + 50, 270)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_2"), new Vector2(_NewX, 270)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_4"), new Vector2(_NewX + 50, 450)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_4"), new Vector2(_NewX+100, 450)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_3"), new Vector2(_NewX+ 100, 450)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_2"), new Vector2(_NewX, 450)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_3"), new Vector2(_NewX + 450, 450)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_1"), new Vector2(_NewX, 450)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_2"), new Vector2(_NewX, 450)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            /* ������ 2 ����� */
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_4"), new Vector2(_NewX, 350)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_3"), new Vector2(_NewX, 250)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_2"), new Vector2(_NewX, 250)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_3"), new Vector2(_NewX, 450)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_2"), new Vector2(_NewX, 450)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_3"), new Vector2(_NewX + 100, 250)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_1"), new Vector2(_NewX, 250)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_2"), new Vector2(_NewX, 250)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_3"), new Vector2(_NewX, 450)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_2"), new Vector2(_NewX, 450)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_3"), new Vector2(_NewX + 150, 450)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_1"), new Vector2(_NewX, 450)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_1"), new Vector2(_NewX, 450)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_1"), new Vector2(_NewX, 450)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_1"), new Vector2(_NewX, 450)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            _Platforms.Add(new Platform(Content.Load<Texture2D>("Platforma_1"), new Vector2(_NewX, 450)));
            _NewX = _Platforms[_Platforms.Count - 1].Position.X + _Platforms[_Platforms.Count - 1].FrameWidth;
            /* ������������� ����� */
            if (_Points.Count > 0) _Points.Clear();
            for (int i = 0; i < 50; i++)
            {
                _Points.Add(new Points(Content.Load<Texture2D>("Points"), new Vector2(0, 0)));
                _Points[i].NewPositionRND(RND);
            }
            /* ������������� ������� */
            if (_Traps.Count > 0) _Traps.Clear();
            _Traps.Add(new Traps(Content.Load<Texture2D>("Trap_1"), new Vector2(7204, 450 - 102)));
            _Traps.Add(new Traps(Content.Load<Texture2D>("Trap_1"), new Vector2(8643, 450 - 102)));
            _Traps.Add(new Traps(Content.Load<Texture2D>("Trap_1"), new Vector2(9003, 450 - 102)));
            _Traps.Add(new Traps(Content.Load<Texture2D>("Trap_1"), new Vector2(10491, 450 - 102)));
            _Traps.Add(new Traps(Content.Load<Texture2D>("Trap_1"), new Vector2(10851, 450 - 102)));
            _Traps.Add(new Traps(Content.Load<Texture2D>("Trap_1"), new Vector2(11900, 450 - 102)));
            /* ������������� ���������� ��� ������� */
            _SecTime = 0;
            _Time = 0;
            /* ������������� ���������� ��� ���������� */
            _FallingTime = 0;
            _FlagFalling = true;
            _FlagFallingP = false;
        }
        protected override void UnloadContent()
        { }
        protected override void Update(GameTime _GameTime)
        {
            MouseState _MouseState = Mouse.GetState();
            _KeyUP = _KeyDown;
            _KeyDown = Keyboard.GetState();
            switch (CurrentGame)
            {
                case Structure_Game.Menu:
                    switch (CurrentMenu)
                    {
                        case Structure_Menu.Global:
                            if (FlagStart[0] == false)
                            {
                                MediaPlayer.Play(Sound[0]);
                                MediaPlayer.IsRepeating = true;
                                MediaPlayer.Volume = 0.3f;
                                FlagStart[0] = true;
                            }
                            for (int n = 0; n < 3; n++) _Buttons[n].Update(_MouseState);
                            if (_Buttons[1].isClicked == true) CurrentMenu = Structure_Menu.Rating;
                            if (_Buttons[2].isClicked == true) Exit();
                            if (_Buttons[0].isClicked == true)
                            {
                                if ((_InputText.FullText.Length <= 7) && (_InputText.FullText.Length > 0))
                                {
                                    _Player.Name = _InputText.FullText;
                                    CurrentGame = Structure_Game.Play;
                                }
                            }
                            _InputText.Update(_GameTime);
                            break;
                        case Structure_Menu.Rating:
                            _Buttons[3].Update(_MouseState);
                            if (_Buttons[3].isClicked == true) CurrentMenu = Structure_Menu.Global;
                            break;
                    }
                    break;
                case Structure_Game.Play:
                    if (FlagStart[1] == false)
                    {
                        MediaPlayer.Play(Sound[1]);
                        MediaPlayer.IsRepeating = true;
                        MediaPlayer.Volume = 0.3f;
                        FlagStart[1] = true;
                    }
                    _Player.Update(_GameTime);
                    _UI = new UI(Content.Load<Texture2D>("UI"), new Vector2(0, 0), 0, 0, Content.Load<SpriteFont>("TextUI"));
                    _UI.Update(_GameTime, _Player.Name, _Player.HP, _Player.XP, _SecTime);
                    _Camera.Update(_Player.Position);
                    if (_Player.Status == "Dead") CurrentGame = Structure_Game.Final;
                    if (_Player.Position.X >= 12500) CurrentGame = Structure_Game.Final;
                    /* ������ ������� */
                    if (_FlagFalling)
                    {
                        _FallingTime += (float)_GameTime.ElapsedGameTime.TotalMilliseconds;
                        if (_FallingTime > 1)
                        {
                            _Player.Position.Y += 8;
                            _FallingTime = 0;
                        }
                    }
                    _FlagFallingP = false;
                    /* ������ �� ������� */
                    if (_Player.Position.Y > 650)
                    {
                        _Player.Position = new Vector2(150, 0);
                        if (!_Player.FlagImmunity)
                        {
                            _Player.HP--;
                            _Player.FlagImmunity = true;
                        }
                    }
                    /* ������ ��� ������ */
                    _Time += (float)_GameTime.ElapsedGameTime.TotalSeconds;
                    if (_Time > 1)
                    {
                        _SecTime++;
                        _Time = 0;
                    }
                    _KeyUP = _KeyDown;
                    _KeyDown = Keyboard.GetState();
                    /* �������� ��� 1 ����� */
                    if (_Player.Position.X < 7000) _Player.SpeedPrimary = 4;
                    /* �������� ��� 2 ����� */
                    if (_Player.Position.X >= 7000) _Player.SpeedPrimary = 8;
                    /* ���������� �������� */
                    for (int i = 0; i < _Platforms.Count; i++)
                    {
                        _Platforms[i].Update(_GameTime);
                        if ((_Player.Rectangle_Interaction.Bottom >= _Platforms[i].Rectangle_Interaction.Top) &&
                            (_Player.Rectangle_Interaction.Bottom <= _Platforms[i].Rectangle_Interaction.Bottom) &&
                            (_Player.Rectangle_Interaction.Right >= _Platforms[i].Rectangle_Interaction.Left) &&
                            (_Player.Rectangle_Interaction.Left <= _Platforms[i].Rectangle_Interaction.Right))
                        {
                            _FlagFalling = false;
                            _FlagFallingP = true;
                            _Player.FlagJumpDown = true;
                        }
                        else
                        {
                            if (!_FlagFallingP)
                            {
                                _FlagFalling = true;
                                _Player.FlagJumpDown = false;
                            }
                        }
                    }
                    /* ���������� ����� */
                    for (int i = 0; i < _Points.Count; i++)
                    {
                        _Points[i].Update(_GameTime);
                        if (_Points[i].Rectangle_Interaction.Intersects(_Player.Rectangle_Interaction))
                        {
                            _Player.XP++;
                            _Points[i].FlagUse = true;
                            _Points[i].Delete();
                        }
                    }
                    /* ���������� ������� */
                    for (int i = 0; i < _Traps.Count; i++)
                    {
                        _Traps[i].Update(_GameTime);
                        if (_Traps[i].Rectangle_Interaction.Intersects(_Player.Rectangle_Interaction))
                        {
                            if (!_Player.FlagImmunity)
                            {
                                _Player.HP--;
                                _Player.FlagImmunity = true;
                            }
                        }
                    }
                    break;
                case Structure_Game.Final:
                    switch (_Player.Status)
                    {
                        case "Live":
                            if (_KeyDown.IsKeyDown(Keys.Enter) && _KeyUP.IsKeyUp(Keys.Enter))
                            {
                                CurrentGame = Structure_Game.Menu;
                                SaveResult(_Player.Name, _Player.XP);
                                LoadContent();
                            }
                            break;
                        case "Dead":
                            if (_KeyDown.IsKeyDown(Keys.Enter) && _KeyUP.IsKeyUp(Keys.Enter))
                            {
                                CurrentGame = Structure_Game.Menu;
                                SaveResult(_Player.Name, _Player.XP);
                                LoadContent();
                            }
                            break;
                        default:
                            break;
                    }
                    break;
            }

            base.Update(_GameTime);
        }
        /* ���������� �������� */
        protected override void Draw(GameTime _GameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            MouseState _MouseState = Mouse.GetState();
            switch (CurrentGame)
            {
                case Structure_Game.Menu:
                    _SpriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null);
                    switch (CurrentMenu)
                    {
                        case Structure_Menu.Global:
                            _SpriteBatch.Draw(Content.Load<Texture2D>("BackgroundMenu"), new Rectangle(0, 0, 1024, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                            for (int n = 0; n < 3; n++) _Buttons[n].Draw(_SpriteBatch);
                            _InputText.Draw(_SpriteBatch);
                            break;
                        case Structure_Menu.Rating:
                            LoadResult();
                            _SpriteBatch.Draw(Content.Load<Texture2D>("BackgroundRating"), new Rectangle(0, 0, 1024, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                            _Buttons[3].Draw(_SpriteBatch);
                            break;
                    }
                    _SpriteBatch.End();
                    break;
                case Structure_Game.Play:
                    // ��������� UI
                    _GraphicsDeviceManager.GraphicsDevice.Viewport = _ViewportUI;
                    _SpriteBatch.Begin();
                    _UI.Draw(_SpriteBatch);
                    _SpriteBatch.End();
                    // ��������� �������
                    _GraphicsDeviceManager.GraphicsDevice.Viewport = _ViewportGame;
                    _SpriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, _Camera._MatrixTransform);
                    _Player.Draw(_SpriteBatch, Content.Load<SpriteFont>("InputText"));
                    /* ��������� ���� � ������ */
                    int _PushX = 0;
                    for (int i = 0; i < 15; i++)
                    {
                        _SpriteBatch.Draw(Content.Load<Texture2D>("BackgroundLvL"), new Rectangle(_PushX, 0, 1024, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                        _PushX += 1024;
                    }
                    /* ��������� �������� */
                    for (int i = 0; i < _Platforms.Count; i++) _Platforms[i].Draw(_SpriteBatch);
                    /* ��������� ����� */
                    for (int i = 0; i < _Points.Count; i++) _Points[i].Draw(_SpriteBatch);
                    /* ��������� ������� */
                    for (int i = 0; i < _Traps.Count; i++) _Traps[i].Draw(_SpriteBatch);
                    /* ��������� ������ ���������� �������� */
                    _SpriteBatch.Draw(Content.Load<Texture2D>("Other_1"), new Rectangle(200, 100, 300, 500), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.69f);
                    _SpriteBatch.Draw(Content.Load<Texture2D>("Other_2"), new Rectangle(671, 230, 111, 390), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.69f);
                    _SpriteBatch.Draw(Content.Load<Texture2D>("Other_3"), new Rectangle(600, 110, 37, 129), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.69f);
                    _SpriteBatch.Draw(Content.Load<Texture2D>("Other_4"), new Rectangle(500, 45, 305, 80), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.69f);
                    _SpriteBatch.Draw(Content.Load<Texture2D>("Other_3"), new Rectangle(7000, 330, 37, 129), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.69f);
                    _SpriteBatch.Draw(Content.Load<Texture2D>("Other_5"), new Rectangle(6900, 265, 305, 80), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.69f);
                    _SpriteBatch.Draw(Content.Load<Texture2D>("Other_3"), new Rectangle(12370, 330, 37, 129), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.69f);
                    _SpriteBatch.Draw(Content.Load<Texture2D>("Other_6"), new Rectangle(12270, 250, 305, 80), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.69f);
                    _SpriteBatch.End();
                    break;
                case Structure_Game.Final:
                    _GraphicsDeviceManager.GraphicsDevice.Viewport = _ViewportMenu;
                    _SpriteBatch.Begin();
                    switch (_Player.Status)
                    {
                        case "Live":
                            _SpriteBatch.Draw(Content.Load<Texture2D>("BackgroundEndWin"), new Rectangle(0, 0, 1024, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                            break;
                        case "Dead":
                            _SpriteBatch.Draw(Content.Load<Texture2D>("BackgroundEndFail"), new Rectangle(0, 0, 1024, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                            break;
                        default:
                            break;
                    }
                    _SpriteBatch.End();
                    break;
            }
            base.Draw(_GameTime);
        }
        /* ���������� ����������� */
        void SaveResult(string _PlayerName, int _PlayerXP)
        {
            string _PlayerDate = DateTime.Now.Day.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString();
            string _CurrentDirectory = Environment.CurrentDirectory;
            FileStream _FileStream;
            XmlDocument _XmlDocument;
            if (File.Exists(_CurrentDirectory + @"\Save.xml"))
            {
                _FileStream = new FileStream(_CurrentDirectory + @"\Save.xml", FileMode.Open);
                _XmlDocument = new XmlDocument();
                _XmlDocument.Load(_FileStream);
                _FileStream.Close();
                _FileStream = new FileStream(_CurrentDirectory + @"\Save.xml", FileMode.Create);
                if (_XmlDocument["Game"].ChildNodes.Count <= 5)
                {
                    XmlNode _XmlNode = _XmlDocument.CreateElement("Save");
                    XmlAttribute _XmlAttribute1 = _XmlDocument.CreateAttribute("Name");
                    _XmlAttribute1.Value = _PlayerName;
                    _XmlNode.Attributes.Append(_XmlAttribute1);
                    XmlAttribute _XmlAttribute2 = _XmlDocument.CreateAttribute("XP");
                    _XmlAttribute2.Value = _PlayerXP.ToString();
                    _XmlNode.Attributes.Append(_XmlAttribute2);
                    XmlAttribute _XmlAttribute3 = _XmlDocument.CreateAttribute("Date");
                    _XmlAttribute3.Value = _PlayerDate;
                    _XmlNode.Attributes.Append(_XmlAttribute3);
                    _XmlDocument.DocumentElement.AppendChild(_XmlNode);
                }
                else
                {
                    _XmlDocument["Game"].RemoveAll();
                    XmlNode _XmlNode = _XmlDocument.CreateElement("Save");
                    XmlAttribute _XmlAttribute1 = _XmlDocument.CreateAttribute("Name");
                    _XmlAttribute1.Value = _PlayerName;
                    _XmlNode.Attributes.Append(_XmlAttribute1);
                    XmlAttribute _XmlAttribute2 = _XmlDocument.CreateAttribute("XP");
                    _XmlAttribute2.Value = _PlayerXP.ToString();
                    _XmlNode.Attributes.Append(_XmlAttribute2);
                    XmlAttribute _XmlAttribute3 = _XmlDocument.CreateAttribute("Date");
                    _XmlAttribute3.Value = _PlayerDate;
                    _XmlNode.Attributes.Append(_XmlAttribute3);
                    _XmlDocument.DocumentElement.AppendChild(_XmlNode);
                }
                _XmlDocument.Save(_FileStream);
                _FileStream.Close();
            }
            else
            {
                _FileStream = new FileStream(_CurrentDirectory + @"\Save.xml", FileMode.CreateNew);
                _XmlDocument = new XmlDocument();
                _XmlDocument.LoadXml("<Game></Game>");
                XmlNode _XmlNode = _XmlDocument.CreateElement("Save");
                XmlAttribute _XmlAttribute1 = _XmlDocument.CreateAttribute("Name");
                _XmlAttribute1.Value = _PlayerName;
                _XmlNode.Attributes.Append(_XmlAttribute1);
                XmlAttribute _XmlAttribute2 = _XmlDocument.CreateAttribute("XP");
                _XmlAttribute2.Value = _PlayerXP.ToString();
                _XmlNode.Attributes.Append(_XmlAttribute2);
                XmlAttribute _XmlAttribute3 = _XmlDocument.CreateAttribute("Date");
                _XmlAttribute3.Value = _PlayerDate;
                _XmlNode.Attributes.Append(_XmlAttribute3);
                _XmlDocument.DocumentElement.AppendChild(_XmlNode);
                _XmlDocument.Save(_FileStream);
                _FileStream.Close();
            }
        }
        /* �������� ����������� */
        void LoadResult()
        {
            string _CurrentDirectory = Environment.CurrentDirectory;
            if (File.Exists(_CurrentDirectory + @"\Save.xml"))
            {
                string _PlayerName = "";
                string _PlayerXP = "";
                string _PlayerDate = "";
                int _interval = 50;
                FileStream _FileStream = new FileStream(_CurrentDirectory + @"\Save.xml", FileMode.Open);
                XmlDocument _XmlDocument = new XmlDocument();
                _XmlDocument.Load(_FileStream);
                _FileStream.Close();
                for (int i = 0; i < _XmlDocument["Game"].ChildNodes.Count; i++)
                {
                    _PlayerName = _XmlDocument["Game"].ChildNodes[i].Attributes["Name"].InnerText;
                    _PlayerXP = _XmlDocument["Game"].ChildNodes[i].Attributes["XP"].InnerText;
                    _PlayerDate = _XmlDocument["Game"].ChildNodes[i].Attributes["Date"].InnerText;
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextUI"), _PlayerName, new Vector2(100, 70 + _interval), Color.Black);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextUI"), _PlayerXP, new Vector2(450, 70 + _interval), Color.Black);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextUI"), _PlayerDate, new Vector2(700, 70 + _interval), Color.Black);
                    _interval += 50;
                }
            }
        }
    }
}
