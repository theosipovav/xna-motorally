﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MotoRally
{
    class InputText
    {
        private string Symbol = "Null";
        private int SizeString = 12;
        private SpriteFont Text_UI;
        private Vector2 Position;
        private KeyboardState KeyUP, KeyDown;
        public string FullText = "";

        public InputText(Vector2 newPosition, SpriteFont newTText_UI)
        {
            Position = newPosition;
            Text_UI = newTText_UI;
        }

        public void Update(GameTime _InGametime)
        {
            KeyUP = KeyDown;
            KeyDown = Keyboard.GetState();
            if (KeyDown.IsKeyDown(Keys.Q) && KeyUP.IsKeyUp(Keys.Q)) Symbol = "Й";
            if (KeyDown.IsKeyDown(Keys.W) && KeyUP.IsKeyUp(Keys.W)) Symbol = "Ц";
            if (KeyDown.IsKeyDown(Keys.E) && KeyUP.IsKeyUp(Keys.E)) Symbol = "У";
            if (KeyDown.IsKeyDown(Keys.R) && KeyUP.IsKeyUp(Keys.R)) Symbol = "К";
            if (KeyDown.IsKeyDown(Keys.T) && KeyUP.IsKeyUp(Keys.T)) Symbol = "Е";
            if (KeyDown.IsKeyDown(Keys.Y) && KeyUP.IsKeyUp(Keys.Y)) Symbol = "Н";
            if (KeyDown.IsKeyDown(Keys.U) && KeyUP.IsKeyUp(Keys.U)) Symbol = "Г";
            if (KeyDown.IsKeyDown(Keys.I) && KeyUP.IsKeyUp(Keys.I)) Symbol = "Ш";
            if (KeyDown.IsKeyDown(Keys.O) && KeyUP.IsKeyUp(Keys.O)) Symbol = "Щ";
            if (KeyDown.IsKeyDown(Keys.P) && KeyUP.IsKeyUp(Keys.P)) Symbol = "З";
            if (KeyDown.IsKeyDown(Keys.A) && KeyUP.IsKeyUp(Keys.A)) Symbol = "Ф";
            if (KeyDown.IsKeyDown(Keys.S) && KeyUP.IsKeyUp(Keys.S)) Symbol = "Ы";
            if (KeyDown.IsKeyDown(Keys.D) && KeyUP.IsKeyUp(Keys.D)) Symbol = "В";
            if (KeyDown.IsKeyDown(Keys.F) && KeyUP.IsKeyUp(Keys.F)) Symbol = "А";
            if (KeyDown.IsKeyDown(Keys.G) && KeyUP.IsKeyUp(Keys.G)) Symbol = "П";
            if (KeyDown.IsKeyDown(Keys.H) && KeyUP.IsKeyUp(Keys.H)) Symbol = "Р";
            if (KeyDown.IsKeyDown(Keys.J) && KeyUP.IsKeyUp(Keys.J)) Symbol = "О";
            if (KeyDown.IsKeyDown(Keys.K) && KeyUP.IsKeyUp(Keys.K)) Symbol = "Л";
            if (KeyDown.IsKeyDown(Keys.L) && KeyUP.IsKeyUp(Keys.L)) Symbol = "Д";
            if (KeyDown.IsKeyDown(Keys.Z) && KeyUP.IsKeyUp(Keys.Z)) Symbol = "Я";
            if (KeyDown.IsKeyDown(Keys.X) && KeyUP.IsKeyUp(Keys.X)) Symbol = "Ч";
            if (KeyDown.IsKeyDown(Keys.C) && KeyUP.IsKeyUp(Keys.C)) Symbol = "С";
            if (KeyDown.IsKeyDown(Keys.V) && KeyUP.IsKeyUp(Keys.V)) Symbol = "М";
            if (KeyDown.IsKeyDown(Keys.B) && KeyUP.IsKeyUp(Keys.B)) Symbol = "И";
            if (KeyDown.IsKeyDown(Keys.N) && KeyUP.IsKeyUp(Keys.N)) Symbol = "Т";
            if (KeyDown.IsKeyDown(Keys.M) && KeyUP.IsKeyUp(Keys.M)) Symbol = "Ь";
            if (KeyDown.IsKeyDown(Keys.OemOpenBrackets) && KeyUP.IsKeyUp(Keys.OemOpenBrackets)) Symbol = "Х";
            if (KeyDown.IsKeyDown(Keys.OemCloseBrackets) && KeyUP.IsKeyUp(Keys.OemCloseBrackets)) Symbol = "Ъ";
            if (KeyDown.IsKeyDown(Keys.OemComma) && KeyUP.IsKeyUp(Keys.OemComma)) Symbol = "Б";
            if (KeyDown.IsKeyDown(Keys.OemPeriod) && KeyUP.IsKeyUp(Keys.OemPeriod)) Symbol = "Ю";
            if (KeyDown.IsKeyDown(Keys.OemSemicolon) && KeyUP.IsKeyUp(Keys.OemSemicolon)) Symbol = "Ж";
            if (KeyDown.IsKeyDown(Keys.OemQuotes) && KeyUP.IsKeyUp(Keys.OemQuotes)) Symbol = "Э";
            if (KeyDown.IsKeyDown(Keys.Back) && KeyUP.IsKeyUp(Keys.Back)) if (FullText.Length != 0) FullText = FullText.Substring(0, FullText.Length - 1);
            if ((Symbol != "Null") && (FullText.Length < SizeString))
            {
                FullText = FullText + Symbol;
                Symbol = "Null";
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(Text_UI, FullText, Position, Color.Black);
        }

        public void Delete_FullText()
        {
            FullText = "";
        }
    }
}
