﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MotoRally
{
    class Buttons
    {
        private Texture2D Texture;
        private Vector2 Position, Original_position;
        private Rectangle Rectangle, Rectangle_Interaction, MouseRectangle;
        private Color Colour = new Color(255, 255, 255, 255);
        private int FrameHeight, FrameWidth, FrameCurrent;
        public Buttons(Texture2D newTexture, Vector2 newPosition)
        {
            Texture = newTexture;
            Position = newPosition;
            FrameHeight = Texture.Height;
            FrameWidth = Texture.Width / 2;
        }
        public bool isClicked;
        public void Update(MouseState mouse)
        {
            MouseRectangle = new Rectangle(mouse.X, mouse.Y, 1, 1);
            Rectangle = new Rectangle(FrameCurrent * FrameWidth, 0, FrameWidth, FrameHeight);
            Rectangle_Interaction = new Rectangle((int)Position.X - FrameWidth / 2, (int)Position.Y - FrameHeight / 2, FrameWidth, FrameHeight);
            Original_position = new Vector2(Rectangle.Width / 2, Rectangle.Height / 2);
            if (MouseRectangle.Intersects(Rectangle_Interaction))
            {
                FrameCurrent = 1;
                if (mouse.LeftButton == ButtonState.Pressed) isClicked = true;
            }
            else
            {
                FrameCurrent = 0;
                isClicked = false;
            }
        }
        public void Draw(SpriteBatch _SpriteBatch)
        {
            _SpriteBatch.Draw(Texture, Position, Rectangle, Colour, 0f, Original_position, 1.0f, SpriteEffects.None, 0.1f);
        }

        public void toMod_Position(Vector2 newMod)
        {
            Position = newMod;
        }

    }
}
